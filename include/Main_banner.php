    <div class="view jarallax  animated fadeIn" style="background-image: url(https://mdbootstrap.com/img/Photos/Others/gradient3.png); background-repeat: no-repeat; background-size: cover; background-position: center center;">
      <div class="mask rgba-indigo-slight">
        <div class="container h-100 d-flex justify-content-center align-items-center">
          <div class="row pt-5 mt-3">
            <div class="col-md-12 mb-3">
              <div class="intro-info-content text-center">
                <h1 class="display-3 blue-text mb-5 wow  animated fadeInDown" data-wow-delay="0.3s">
                  <a class="blue-text font-weight-bold">M-Calendar</a>
                </h1>
                <h5 class="text-uppercase blue-text mb-5 mt-1 font-weight-bold wow fadeInDown animated" data-wow-delay="0.3s">The best calendar events!</h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!--Main Navigation-->
