    </div>
  </main>
  <!--Main Layout-->
<footer class="page-footer mdb-color lighten-3 text-center text-md-left">

    <!--Copyright-->
    <div class="footer-copyright text-center py-3 animated fadeIn">
      <div class="container-fluid">
        Copyright &copy; 2020 MyCalendar.
      </div>
    </div>
    <!--/Copyright-->
</footer>
  <!--/Footer-->

  <!-- jQuery -->
  <script type="text/javascript" src="<?php echo $config["setting"]["website"]["website_url"]; ?>dist/js/jquery.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="<?php echo $config["setting"]["website"]["website_url"]; ?>dist/js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="<?php echo $config["setting"]["website"]["website_url"]; ?>dist/js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="<?php echo $config["setting"]["website"]["website_url"]; ?>dist/js/mdb.min.js"></script>
  <!-- Your custom scripts (optional) -->
  <script type="text/javascript"></script>

</body>
</html>