<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title><?php echo $config["setting"]["website"]["website_title"]; ?></title>
  <!-- MDB icon -->
  <link rel="icon" href="img/favicon.ico" type="image/x-icon">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
  <!-- Google Fonts Roboto -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="<?=$config["setting"]["website"]['website_url'];?>dist/css/bootstrap.min.css">
  <!-- Material Design Bootstrap -->
  <link rel="stylesheet" href="<?=$config["setting"]["website"]['website_url'];?>dist/css/mdb.min.css">
  <!-- Your custom styles (optional) -->
  <link rel="stylesheet" href="<?=$config["setting"]["website"]['website_url'];?>dist/css/style.css">
  <!-- Calendar CSS -->
  <link rel="stylesheet" href="<?=$config["setting"]["website"]['website_url'];?>dist/css/kencalendar.css">

  <link href="<?=$config["setting"]["website"]['website_url'];?>dist/sweetalert/dist/sweetalert.css" rel="stylesheet">
  <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
  <script src="<?=$config["setting"]["website"]['website_url'];?>dist/sweetalert/dist/sweetalert.min.js"></script>
  
  <link href="<?=$config["setting"]["website"]['website_url'];?>dist/sweetalert/dist/sweetalert.css" rel="stylesheet">
  <script src="<?=$config["setting"]["website"]['website_url'];?>dist/sweetalert/dist/sweetalert.min.js"></script>
</head>
<!--Main Layout-->
<body>

  
  