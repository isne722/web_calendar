<div class="container animated fadeIn">
  <!-- Default form login -->
      <form class="text-center p-5" method="POST" action="?page=signin&act=login">

          <p class="h4 mb-4">Sign in</p>


          <input type="text" name="email" id="email" class="form-control mb-4" placeholder="E-mail">
          <input type="password" name="password" id="password" class="form-control mb-4" placeholder="Password">
          <button class="btn btn-info btn-block my-4  animated fadeIn" type="submit">Sign in</button>

          <!-- Register -->
          <p>Not a member?
              <a href="?page=signup">Register</a>
          </p>

      </form>
  <!-- Default form login -->
</div>

      <?php 

      if(isset($_GET["act"])){
        if($_GET["act"] == "login"){
          $user_query = $pdo->prepare("SELECT * FROM users WHERE email = :email");
          $user_query->execute(array(":email" => $_POST["email"]));
          if($user_query->rowCount()){
            $user = $user_query->fetch(PDO::FETCH_ASSOC);
            if(md5($_POST["password"]) == $user["password"]){
              $_SESSION["email"] = $user["email"];
              $_SESSION["user_id"] = $user["id"];
              $_SESSION["login_member"] = true;
              $user_insert = $pdo->prepare("UPDATE users SET last_login_ip = :last_login_ip,login_date = :login_date WHERE email = :email");
              $user_insert->execute(array(":last_login_ip" => $_SERVER["REMOTE_ADDR"],":login_date" => date('Y-m-d') , ":email" => $_POST["email"]));
              //echo '<script>alert("เข้าสู่ระบบสำเร็จ");window.location.href="member/?page=home";</script>';
              alert('Login Success','success','member/?page=home');
            }else{
              //echo '<script>alert("รหัสผ่านไม่ถูกต้อง");window.location.href="member/?page=home";</script>';
              alert('User Not Found','error','member/?page=home');
            }
          }else{
            //echo '<script>alert("ไม่พบชื่อผู้ใช้งาน");window.location.href="member/?page=home";</script>';
            alert('User Not Found','error','member/?page=home');
          }
        }
      }