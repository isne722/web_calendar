<div class="container animated fadeIn">
    <!-- Default form register -->
    <form class="text-center p-5" method="POST" action="?page=signup&act=register">

        <p class="h4 mb-4">Sign up</p>

        <div class="form-row mb-4">
            <div class="col">
                <!-- First name -->
                <input type="text" id="firstname" name="firstname" class="form-control" placeholder="First name">
            </div>
            <div class="col">
                <!-- Last name -->
                <input type="text" id="lastname" name="lastname" class="form-control" placeholder="Last name">
            </div>
        </div>

        <!-- E-mail -->
        <input type="email" id="email" name="email" class="form-control mb-4" placeholder="E-mail">

        <!-- Password -->
        <input type="password" id="password" name="password" class="form-control" placeholder="Password" min="8">
        <small class="form-text text-muted mb-4">
            At least 8 characters and 1 digit
        </small>

        <!-- Phone number -->
        <input type="number" id="phone" name="tel" class="form-control" placeholder="Phone number" min="10">
        <small class="form-text text-muted mb-4">
            Optional - for two step authentication
        </small>

        <!-- Sign up button -->
        <button class="btn btn-info my-4 btn-block" type="submit">Sign up</button>

        <!-- Terms of service -->
        <p>Already have account
            <em><a href="?page=signin">Sign in</a></em>

    </form>
</div>
<!-- Default form register -->

      <?php 

      if(isset($_GET["act"])){
        if($_GET["act"] == "register"){
            if($_POST["nlt"] == true){
                $ntl = 1;
            }else{
                $ntl = 0;
            }

          $user_query = $pdo->prepare("SELECT password FROM users WHERE email = :email OR firstName = :firstName OR lastName = :lastName");
          $user_query->execute(array(":tel" => $_POST["tel"],":email" => $_POST["email"],":firstName" => $_POST["firstName"],":lastName" => $_POST["lastName"]));


          if($user_query->rowCount()){
            //echo '<script>alert("ข้อมูลดังกล่างมีผู้ใช้งานไปแล้ว");window.location.href="?page=signup";</script>';
            alert('This email has been taken','error','?page=signup');
          }else{
            if(strlen($_POST["password"]) < 8){
                //echo '<script>alert("รหัสผ่านของคุณน้อยกว่า 8 ตัว");window.location.href="?page=signup";</script>';
                alert('Password at least 8 characters and 1 digit','error','?page=signup');
            }elseif(strlen($_POST["tel"]) != 10){
                //echo '<script>alert("เบอร์โทรของคุณน้อยกว่า 10 ตัว");window.location.href="?page=signup";</script>';
                alert('Phone Number at least 10 letters','error','?page=signup');
            }else{
                $insert_query = $pdo->prepare("INSERT INTO users (email,password,firstname,lastname,nlt,tel) VALUES (:email,:password,:firstname,:lastname,:nlt,:tel)");
                $insert_query->execute(array(
                    ":email" => $_POST["email"],
                    ":password" => md5($_POST["password"]),
                    ":firstname" => $_POST["firstname"],
                    ":lastname" => $_POST["lastname"],
                    ":tel" => $_POST["tel"],
                    ":nlt" => $ntl
                ));
                //echo '<script>alert("สมัครสมาชิกสำเร็จ");window.location.href="?page=signup";</script>';
                alert('Register Complete','success','?page=signin');
            }
          }
        }
      }