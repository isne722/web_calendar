    

  <!--Main Layout-->
<footer class="page-footer mdb-color lighten-3 text-center text-md-left">

    <!--Copyright-->
    <div class="footer-copyright text-center py-3">
      <div class="container-fluid">
        Copyright &copy; 2020 MyCalendar.
      </div>
    </div>
    <!--/Copyright-->
</footer>
  <!--/Footer-->
  <!-- Your custom scripts (optional) -->
  <script type="text/javascript"></script>

</body>
</html>