<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title><?php echo $config["setting"]["website"]["website_title"]; ?></title>
  <!-- MDB icon -->
  <link rel="icon" href="<?=$config["setting"]["website"]['website_url'];?>img/favicon.ico" type="image/x-icon">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
  <!-- Google Fonts Roboto -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="<?=$config["setting"]["website"]['website_url'];?>dist/css/bootstrap.min.css">
  <!-- Material Design Bootstrap -->
  <link rel="stylesheet" href="<?=$config["setting"]["website"]['website_url'];?>dist/css/mdb.min.css">
  <!-- Your custom styles (optional) -->
  <link rel="stylesheet" href="<?=$config["setting"]["website"]['website_url'];?>dist/css/style.css">
  <!-- Calendar CSS -->
  <link rel="stylesheet" href="<?=$config["setting"]["website"]['website_url'];?>dist/css/kencalendar.css">
  
  <!-- jQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="<?php echo $config["setting"]["website"]["website_url"]; ?>dist/js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="<?php echo $config["setting"]["website"]["website_url"]; ?>dist/js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="<?php echo $config["setting"]["website"]["website_url"]; ?>dist/js/mdb.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

  <link href="<?=$config["setting"]["website"]['website_url'];?>dist/sweetalert/dist/sweetalert.css" rel="stylesheet">
  <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
  <script src="<?=$config["setting"]["website"]['website_url'];?>dist/sweetalert/dist/sweetalert.min.js"></script>
  
  <link href="<?=$config["setting"]["website"]['website_url'];?>dist/sweetalert/dist/sweetalert.css" rel="stylesheet">
  <script src="<?=$config["setting"]["website"]['website_url'];?>dist/sweetalert/dist/sweetalert.min.js"></script>
  
</head>
<!--Main Layout-->
<body>
<script>
function post_ajax(s,c=[]){
 	//alert(id);
	const swalWithBootstrapButtons = Swal.mixin({
	  buttonsStyling: true,
	})

	swalWithBootstrapButtons.fire({
	  title: 'Alert',
	  text: "Confirm?",
	  icon: 'warning',
  showClass: {
    popup: 'animated fadeInDown faster'
  },
  hideClass: {
    popup: 'animated fadeOutUp faster'
  },
	  showCancelButton: true,
	  confirmButtonText: 'Confirm',
	  cancelButtonText: 'Cancel',
	  reverseButtons: true
	}).then((result) => {
	  if (result.value) {
		    $.post( s, c, function(data) {
		               	if(data.type == "success"){
		               		swalWithBootstrapButtons.fire('Alert','ทำรายการสำเร็จ','success');
							$('#display_calender').load("../include/ajax/ajax_calender.php?date="+ data.date);
		               	}
		    },'json');

	  } else if (
	    /* Read more about handling dismissals below */
	    result.dismiss === Swal.DismissReason.cancel
	  ) {
	    swalWithBootstrapButtons.fire(
	      'Alert',
	      'Cancel Complete',
	      'error'
	    )
	  }
	})
}
</script>

  
  