<div class="container">
  <section style="margin-top: 100px">
    <div class="jumbotron animated fadeIn">
      <h4 class="h3-responsive text-center font-weight-bold dark-grey-text" style="margin-top: -20px">My Appointment</h4>
      <hr>
      <div class="row">
        <div class="col-12 col-md-12">
          <div>
            <?php
			        $date_displays = date_create(date('Y-m-d'));
            ?>
			<div id="display_calender"></div>
          </center>
        </div>
      </div>
    </div>
  </section>
</div>
<script type="text/javascript">
	$('#display_calender').load("../include/ajax/ajax_calender.php?date=<?=date('Y-m-d');?>");

	function formatDate(date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();
		if (month.length < 2) 
			month = '0' + month;
		if (day.length < 2) 
			day = '0' + day;
		return [day,month,year].join('-');
	}

	$(document).on("click", ".date_timer", function () {
		var date = $(this).data('date');
		$('#display_calender').load('../include/ajax/ajax_calender.php?date=' + date);
	});
  
</script>
