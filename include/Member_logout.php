<?php

session_start();

session_unset($_SESSION["user_id"]);
session_unset($_SESSION["email"]);
session_unset($_SESSION["login_member"]);

session_destroy();

header("Location: ../?page=signin");