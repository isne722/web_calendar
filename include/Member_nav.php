  <!--Main Navigation-->
  <header>
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar aqua-gradient">
      <div class="container">
        <a class="navbar-brand" href="#">
          <strong>M-Cal</strong>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7"
          aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="?page=home">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="?page=profile">Profiles
                <span class="sr-only">(current)</span>
              </a>
            </li>
          </ul>
          <form class="form-inline">
            <div class="md-form my-0">
              <a href="?page=logout"><button class="btn btn-sm align-middle btn-outline-white" type="button">Logout</button></a>
            </div>
          </form>
        </div>
      </div>
    </nav>
  </header>
  <!--Main Layout-->
