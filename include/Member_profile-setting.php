<section style="margin-top: 100px">
  <div class="container">
    <div class="jumbotron p-5 text-center text-md-left author-box animated fadeIn" data-wow-delay="0.3s" style="visibility: visible; animation-name: fadeIn; animation-iteration-count: 1; animation-delay: 0.3s;">
    <!--Name-->
      <h4 class="h3-responsive text-center font-weight-bold dark-grey-text">Profile Setting</h4>
      <hr>
      <div class="row">
        <!--Author Data-->
        <div class="col-12 col-md-12">
          <form method="POST" action="?page=profile-setting&act=changepro">
          <input type="text" name="firstname" id="firstname" class="form-control mb-4" placeholder="Firstname" value="<?=$results['firstName'];?>" required>
          <input type="text" name="lastname" id="lastname" class="form-control mb-4" placeholder="Lastname" value="<?=$results['lastName'];?>" required>
          <input type="text" name="tel" id="phone" class="form-control mb-4" placeholder="Phone Number" value="<?=$results['tel'];?>" required>
          <button class="btn btn-info btn-block my-4" type="submit">Save Changes</button>
        </form>
        </div>
      </div>
    </div>
  </div>
</section>

      <?php 

      if(isset($_GET["act"])){
        if($_GET["act"] == "changepro"){

          $user_query = $pdo->prepare("SELECT firstName FROM users WHERE email = :email");
          $user_query->execute(array(":email" => $_SESSION["email"]));

          if($user_query->rowCount()){
            $user = $user_query->fetch(PDO::FETCH_ASSOC);
            $update_user = $pdo->prepare("UPDATE users SET firstName = :firstname, lastName = :lastname, tel = :tel WHERE email = :email");
            $update_user->execute(array(
              ":email" => $_SESSION["email"],
              ":firstname" => $_POST["firstname"],
              ":lastname" => $_POST["lastname"],
              ":tel" => $_POST["tel"]
            ));
            alert('Profile Updated','success','?page=profile'); 
          }
        }
      }
      ?>
