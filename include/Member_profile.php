<section style="margin-top: 100px">
  <div class="container">
    <div class="jumbotron p-5 text-center text-md-left author-box animated fadeIn" data-wow-delay="0.3s" style="visibility: visible; animation-name: fadeIn; animation-iteration-count: 1; animation-delay: 0.3s;">
      <!--Name-->
      <h4 class="h3-responsive text-center font-weight-bold dark-grey-text">Your Profiles </h4>
      <hr>
      <div class="row">
        <!--Avatar-->
        <div class="col-12 col-md-2 mb-md-0 mb-4">
          <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(32).jpg" class="img-fluid rounded-circle z-depth-2">
        </div>
        <!--Author Data-->
        <div class="col-12 col-md-10">
          <span>
            Firstname : <?=$results['firstName'];?><br>
            Lastname : <?=$results['lastName'];?><br>
            Phone Number : <?=$results['tel'];?><br>
            Email : <?=$results['email'];?><br>
            Login Date : <?=$results['login_date'];?><br>
            Login IP : <?=$results['last_login_ip'];?><br>
            <a href="?page=profile-setting">Edit Profile</a>
          </span>
        </div>
      </div>
    </div>

    <div class="jumbotron p-5 text-center text-md-left author-box animated fadeIn" data-wow-delay="0.3s" style="visibility: visible; animation-name: fadeIn; animation-iteration-count: 1; animation-delay: 0.3s;">
    <!--Name-->
      <h4 class="h3-responsive text-center font-weight-bold dark-grey-text">Change Password</h4>
      <hr>
      <div class="row">
        <!--Author Data-->
        <div class="col-12 col-md-12">
          <form method="POST" action="?page=profile&act=changepw">
          <input type="password" name="old_password" id="password" class="form-control mb-4" placeholder="Old Password">
          <input type="password" name="new_password" id="password" class="form-control mb-4" placeholder="New Password">
          <input type="password" name="con_password" id="password" class="form-control mb-4" placeholder="Confirm Password">
          <button class="btn btn-info btn-block my-4" type="submit">Change Password</button>
        </form>
        </div>
      </div>
    </div>
  </div>
</section>

      <?php 

      if(isset($_GET["act"])){
        if($_GET["act"] == "changepw"){

          $user_query = $pdo->prepare("SELECT password FROM users WHERE email = :email");
          $user_query->execute(array(":email" => $_SESSION["email"]));

          if($user_query->rowCount()){
            $user = $user_query->fetch(PDO::FETCH_ASSOC);
            if(md5($_POST["old_password"]) != $user["password"]){
              alert('Old Password Incorrect','error','?page=profile');
            }elseif ($_POST["new_password"] != $_POST["con_password"]) {
              alert('Password Not Match','error','?page=profile');
            }elseif(strlen($_POST["new_password"]) < 8) {
              alert('Password At Least 8 Characters','error','?page=profile');
            }elseif (strlen($_POST["con_password"]) < 8) {
              alert('Password At Least 8 Characters','?page=profile');
            }else{
              $update_user = $pdo->prepare("UPDATE users SET password = :new_password WHERE email = :email");
              $update_user->execute(array(":email" => $_SESSION["email"],":new_password" => md5($_POST["new_password"])));
              alert('Password Changed','success','?page=profile');
            }
          }else{
            alert('No Username Information','error','?page=profile'); 
          }
        }
      }
      ?>
