<?php
session_start();
require_once (__DIR__).("/../../system/config.php");
require_once (__DIR__).("/../../system/database.php");
    ?>
  <script type="text/javascript" src="<?php echo $config["setting"]["website"]["website_url"]; ?>dist/js/mdb.min.js"></script>
<?php	
  $user_event = $pdo->prepare('SELECT * FROM `tlb_event` WHERE `email` = :email');
  $user_event->execute(array(":email" => $_SESSION["email"]));
  while ($user = $user_event->fetch(PDO::FETCH_ASSOC)) {
    $data[] = $user;
  }
  @$date_now = $_GET["date"] ;
  if(empty($date_now))
  {
		KenCal(date('d-m-Y') ,$data);
  }else{
		KenCal($date_now,$data);
  }

  function KenCal($datas,$data){
    $month = date('m', strtotime($datas));      //Gets month of appointment (1‐12) 
    $year = date('Y', strtotime($datas));      //Gets year of appointment (e.g. 2016) 
	  $firstday = date('w', strtotime('01-' . $month . '-' . $year));  //Gets the day of the week for the 1st of 

     //the month. (e.g. 0 for Sun, 1 for Mon)
    $days = date('t', strtotime($datas));      //Gets number of days in month
    $today = date('d');            //Gets today’s date 
    $todaymonth = date('m');          //Gets today’s month 
    $todayyear = date('Y');            //Gets today’s year 
    $date_display = date_create($datas);
	
    echo '
      <h3 style="margin-top: -10px"><center>
        <button class="btn btn-info btn-sm date_timer" data-date="'.Date("Y-m-d", strtotime($datas." -1 Month")).'"><< Last month</button>
		 '.date_format($date_display,'d-F-Y').'
        <button class="btn btn-info btn-sm date_timer" data-date="'.Date("Y-m-d", strtotime($datas." +1 Month")).'">Next month >></button></center></h3>
	  <div class="calendar" style="margin-left: 95px"> 
        <div class="days">Sunday</div> 
        <div class="days">Monday</div> 
        <div class="days">Tuesday</div> 
        <div class="days">Wednesday</div> 
        <div class="days">Thursday</div> 
        <div class="days">Friday</div> 
        <div class="days">Saturday</div>';

        for($i=1; $i<=$firstday; $i++) { 
          echo '<div class="date blankday"></div>';
        }

        for($i=1; $i<=$days; $i++) {
          echo '
		  <a class="open-AddBookDialog" data-description="" data-name="" data-date="'.$year.'-'.$month.'-'.$i.'" data-date_start="" data-date_end="" data-id="" data-toggle="modal" data-target="#player_editor">
          <div class="date day-hover'; 
          if ($today == $i && $todaymonth==$month && $todayyear == $year) { 
            echo ' today'; 
          }
          echo '">' . $i . '<br><br>';
          foreach ($data as $key => $value) {
            $date_query = date_create($value["date"]);
            $day_q = date('d', strtotime(date_format($date_query,"d-m-Y")));
            $month_q = date('m', strtotime(date_format($date_query,"d-m-Y")));
            $year_q = date('Y', strtotime(date_format($date_query,"d-m-Y")));
              if($day_q == $i && $month_q==$month &&  $todayyear == $year_q) {
                  echo '<center style="bottom: 0;">
						<a 
						href="javascript:void(0);"
						data-name="'.$value['event'].'" 
						data-date_start="'.$value['date_start'].'" 
						data-date_end="'.$value['date_end'].'"
						data-id="'.$value['id'].'"
						data-description="'.$value['description'].'"
						data-toggle="modal"
						data-date="'.$year.'-'.$month.'-'.$i.'" 
						data-target="#player_editor" 
						class="badge open-AddBookDialog badge-primary">View Details</a>
						<p>
						<a href="javascript:void(0);" data-id="'.$value['id'].'" data-dates="'.$year.'-'.$month.'-'.$i.'"
						class="open-delete badge badge-pill badge-danger">Delete</a></p>
						</center>';
			  }
          }
          echo  '</div></a>';
        }
  }
?>
<div class="modal fade" id="player_editor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add / Edit </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  
      	<form onSubmit='post_ajax("<?=$config["setting"]["website"]['website_url'].'system/calender.php?act=update_event'; ?>",$(this).serialize());return false;' method="POST">
      		
			 <label>Date</label>
      		<input type="date" readonly id="date" name="date" class="form-control">			
      		<input type="hidden" readonly id="id" name="id" class="form-control">			
			     <label>Title</label>
      		<input type="text" id="name" name="name" class="form-control">
			Description
      		<input type="text" id="description" name="description" class="form-control">
      		<label>Start</label>
      		<input type="time" name="date_start" id="date_start" class="form-control">
      		<label>End</label>
      		<input type="time" name="date_end" id="date_end" class="form-control">
      		<hr>
      		<div class="modal-footer">
            <button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times-circle"></i> Close</button>  
          </div>	
      	</form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).on("click", ".open-delete", function () {
	var id = $(this).data('id');
	var date = $(this).data('dates');
	const swalWithBootstrapButtons = Swal.mixin({
	  buttonsStyling: true,
	})
	$.ajax({
		type: "POST",
		url: "<?=$config["setting"]["website"]['website_url'].'/system/calender.php?act=delete&id='; ?>"+id + "&date="+date,
		data: '',
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function(data){
		               	if(data.type == "success"){
							swalWithBootstrapButtons.fire('Alert','Success','success');
							$('#display_calender').load("../include/ajax/ajax_calender.php?date="+ data.date);
		               	}
		}
	});
});

$(document).on("click", ".open-AddBookDialog", function () {
     var date_start = $(this).data('date_start');
     var id = $(this).data('id');
     var date = $(this).data('date');
     var date_end = $(this).data('date_end');
     var name = $(this).data('name');
     var description = $(this).data('description');
     $(".modal-body #name").val( name );
     $(".modal-body #date_start").val( date_start );
     $(".modal-body #id").val( id );
     $(".modal-body #date_end").val( date_end );
     $(".modal-body #date").val( date );
     $(".modal-body #description").val( description );
});
</script>
