<?php
  session_start();
  require_once (__DIR__).("/system/config.php");
  require_once (__DIR__).("/system/database.php");

  if(isset($_SESSION["user_id"])){
    echo '<script>window.location.href="member/?page=home";</script>';
    exit();
  }
?>

  <!--Header-->
  <?php require_once (__DIR__).("/include/Main_header.php");?>
  
  <!--Main Layout-->
  <?php require_once (__DIR__).("/include/Main_nav.php");?>
  <?php require_once (__DIR__).("/include/Main_banner.php");?>

  <!--Body-->
  <?php 
    if($_GET["page"] == NULL){
      include_once (__DIR__).("/include/Main_home.php");
    }else{
      include_once (__DIR__).("/include/Main_".$_GET["page"].".php");
    }
  ?>

  <!--Footer-->
  <?php require_once (__DIR__).("/include/Main_footer.php");?>