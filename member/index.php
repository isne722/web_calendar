<?php
  	session_start();
    if(!isset($_SESSION["user_id"])){
      header("location: ../?page=signin");
      exit();
    }
  	require_once (__DIR__).("/../system/config.php");
  	require_once (__DIR__).("/../system/database.php");
  if(isset($_SESSION['email'])){

  $records = $pdo->prepare("SELECT * FROM users WHERE email = '".$_SESSION['email']."'");
  $records->execute();
  $results = $records->fetch(PDO::FETCH_ASSOC);
  }
?>

  	<!--Header-->
  	<?php require_once (__DIR__).("/../include/Member_header.php");?>

  	<!--Main Layout-->
  	<?php require_once (__DIR__).("/../include/Member_nav.php");?>
  	
  	<!--Body-->
  	<?php 
    	if($_GET["page"] == NULL){
      		include (__DIR__).("/../include/Member_home.php");
    	}else{
      		include (__DIR__).("/../include/Member_".$_GET["page"].".php");
    	}
  	?>

  	<!--Footer-->
  	<?php require_once (__DIR__).("/../include/Member_footer.php");?>

