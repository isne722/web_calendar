<?php
session_start();
require_once (__DIR__).("/config.php");
require_once (__DIR__).("/database.php");
@$act = $_GET["act"];
if(!is_null($act)){
	if($act == "update_event"){
		$sel_date = $pdo->prepare('SELECT * FROM `tlb_event` WHERE `date` = :date');
		$sel_date->execute(array(":date" => $_POST["date"]));
		if($sel_date->rowCount()){
				$update_event = $pdo->prepare('UPDATE `tlb_event` SET `event` = :event,`date_start` = :date_start,`date_end` = :date_end`description` = :description WHERE id = :id');
				$update_event->execute(array(":id" => $_POST["id"],":event" => $_POST["name"],":date_start" => $_POST["date_start"],":date_end" => $_POST["date_end"],":description" => $_POST["description"]));
				$data['type'] = 'success';
				$data['date'] = $_POST["date"];
		}else{
			if(empty($_POST["id"])){
				$update_event = $pdo->prepare('INSERT INTO `tlb_event`(`email` ,`description`, `event`, `date`, `date_start`, `date_end`) VALUES (:email,:description,:event,:date,:date_start,:date_end)');
				$update_event->execute(array(":email" => $_SESSION["email"],":event" => $_POST["name"],":date" => $_POST["date"],":date_start" => $_POST["date_start"],":date_end" => $_POST["date_end"],":description" => $_POST["description"]));
				$data['type'] = 'success';
				$data['date'] = $_POST["date"];
			}
		}
	}elseif($act == 'delete'){
				$update_event = $pdo->prepare('DELETE FROM `tlb_event` WHERE id = :id');
				$update_event->execute(array(":id" => $_GET["id"]));
				$data['type'] = 'success';
				$data['date'] = $_GET["date"];
	}
	exit(json_encode($data));
}

?>