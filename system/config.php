<?php 
date_default_timezone_set('Asia/Bangkok');
error_reporting(0);
///===============================================================
$config["setting"]["host"] = array(
	"host_name" => 'localhost',
	"host_user" => 'root',
	"host_password" => '',
	"host_database" => 'calendar',
);

//===============================================================

$config["setting"]["website"] = array(
	"website_url" => 'http://154.16.11.251/calendar3/',
	"website_title" => 'Hello Calendar',
	"website_title_member" => 'Hello Calendar : Member',
	"website_version" => '1.0',
);

//===============================================================

function alert($alert = "",$type = "",$target = ""){
	echo '
			<script>
				swal({
					title: "Alert",
					text: "'.$alert.'",
						type: "'.$type.'",
						confirmButtonText: "OK"
					},
					function(isConfirm){
						if (isConfirm) {
						    window.location.href = "'.$target.'";
						}
					});
			</script>
		';
}